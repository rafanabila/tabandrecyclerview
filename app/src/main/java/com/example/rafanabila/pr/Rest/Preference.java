package com.example.rafanabila.pr.Rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preference {
    static final String KEY_DATA = "Data";
    static final String KEY_EMAIL = "Email";
    static final String KEY_Alamat = "Alamat";


    /**
     * Pendlakarasian Shared Preferences yang berdasarkan paramater context
     */
    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setData(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DATA, data);
        editor.apply();
    }


    public static String getData(Context context) {
        return getSharedPreference(context).getString(KEY_DATA, "");
    }


    public static void deleteData(Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_DATA);
        editor.apply();
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_EMAIL, email);
        editor.apply();
    }


    public static String getEmail(Context context) {
        return getSharedPreference(context).getString(KEY_EMAIL, "");
    }


    public static void deleteEmail(Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_EMAIL);
        editor.apply();
    }

    public static void setAlamat(Context context, String alamat) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_Alamat, alamat);
        editor.apply();
    }


    public static String getAlamat(Context context) {
        return getSharedPreference(context).getString(KEY_Alamat, "");
    }


    public static void deleteAlamat(Context context) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_Alamat);
        editor.apply();
    }
}
