package com.example.rafanabila.pr.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FavoritModel extends RealmObject {
    @PrimaryKey
    private Integer id;
    private String nama;
    private String kategori;
    private String jenis;
    private String foto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
