package com.example.rafanabila.pr;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items {
    @SerializedName("meals")
    private ArrayList<mealList> meals;

    public ArrayList<mealList> getMeals() {
        return meals;
    }

    public class mealList {
        @SerializedName("strMeal")
        private String strMeal;
        @SerializedName("strCategory")
        private String strCategory;
        @SerializedName("strArea")
        private String strArea;
        @SerializedName("strMealThumb")
        private String strMealThumb;

        public String getStrMealThumb() {
            return strMealThumb;
        }

        public String getStrMeal() {
            return strMeal;
        }

        public String getStrCategory() {
            return strCategory;
        }

        public String getStrArea() {
            return strArea;
        }
    }
}
