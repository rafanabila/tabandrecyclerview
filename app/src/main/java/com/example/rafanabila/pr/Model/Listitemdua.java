package com.example.rafanabila.pr.Model;

public class Listitemdua {
    public String judul, genre, harga;
    public int foto;

    public Listitemdua(String judul, String genre, String harga, int foto) {
        this.judul = judul;
        this.genre = genre;
        this.harga = harga;
        this.foto = foto;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
