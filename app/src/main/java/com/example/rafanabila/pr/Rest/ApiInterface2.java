package com.example.rafanabila.pr.Rest;

import com.example.rafanabila.pr.Items2;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface2 {
    @GET("categories.php")
    Call<Items2> getData();
}
