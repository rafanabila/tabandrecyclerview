package com.example.rafanabila.pr.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafanabila.pr.Model.FavoritModel;
import com.example.rafanabila.pr.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterFavorit extends RecyclerView.Adapter<AdapterFavorit.ViewHolder> {
    private Context context;
    private List<FavoritModel> favoritModels;

    public AdapterFavorit(Context context, List<FavoritModel> favoritModels) {
        this.context = context;
        this.favoritModels = favoritModels;
    }

    @NonNull
    @Override
    public AdapterFavorit.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.vertical_item, viewGroup, false);
        return new AdapterFavorit.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFavorit.ViewHolder viewHolder, int i) {
        Log.d("DataData", favoritModels.size() + "");
        //Listitem variable
        final FavoritModel listitemList = favoritModels.get(i);
        Log.d("DataData", listitemList.toString());
        //judul diganti nma yang di list
        viewHolder.strMeal.setText(listitemList.getNama());
        viewHolder.strCategory.setText(listitemList.getKategori());
        viewHolder.strArea.setText(listitemList.getJenis());
        Picasso.get().load(listitemList.getFoto()).into(viewHolder.strMealThumb);
    }

    @Override
    public int getItemCount() {
        return favoritModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView strMeal, strCategory, strArea;
        public ImageView strMealThumb;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            strMeal = view.findViewById(R.id.judul);
            strCategory = view.findViewById(R.id.genre);
            strArea = view.findViewById(R.id.harga);
            strMealThumb = view.findViewById(R.id.foto);
            cardView = view.findViewById(R.id.card);
        }
    }
}
