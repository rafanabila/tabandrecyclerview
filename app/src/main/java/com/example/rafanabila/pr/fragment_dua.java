package com.example.rafanabila.pr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafanabila.pr.Adapter.AdapterFavorit;
import com.example.rafanabila.pr.Model.FavoritModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class fragment_dua extends Fragment {

    List<FavoritModel> favoritModelList;
    RecyclerView recyclerView;
    AdapterFavorit af;
    Realm realm;
    RealmHelper realmHelper;
    FavoritModel favoritModel;
    SwipeRefreshLayout sw;

    public fragment_dua() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View r = inflater.inflate(R.layout.fragment_dua, container, false);
        recyclerView = r.findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        sw = r.findViewById(R.id.swLayout);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        favoritModelList = realmHelper.getAllFavorit();
        af = new AdapterFavorit(getActivity(), favoritModelList);
        recyclerView.setAdapter(af);

//        sw.setOnClickListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {

//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {

//                        sw.setRefreshing(false);

//                        favoritModelList = realmHelper.getAllFavorit();
        //                       af = new AdapterFavorit(getActivity(), favoritModelList);
        //                       recyclerView.setAdapter(af);
//                    }
//                }, 2000;
//            }
        //       });

        return r;
    }

}
