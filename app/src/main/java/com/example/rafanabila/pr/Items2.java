package com.example.rafanabila.pr;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Items2 {
    @SerializedName("categories")
    private ArrayList<Items2.categoriesList> categories;

    public ArrayList<categoriesList> getCategories() {
        return categories;
    }

    public class categoriesList {
        @SerializedName("strCategory")
        private String strCategory;
        @SerializedName("strCategoryThumb")
        private String strCategoryThumb;

        public String getStrCategory() {
            return strCategory;
        }

        public String getStrCategoryThumb() {
            return strCategoryThumb;
        }
    }
}

