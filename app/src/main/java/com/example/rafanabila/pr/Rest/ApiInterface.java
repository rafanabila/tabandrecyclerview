package com.example.rafanabila.pr.Rest;

import com.example.rafanabila.pr.Items;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("latest.php")
    Call<Items> getData();
}
