package com.example.rafanabila.pr;

import android.util.Log;

import com.example.rafanabila.pr.Model.FavoritModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmHelper {
    Realm realm;

    public RealmHelper(Realm realm) {
        this.realm = realm;
    }

    // untuk menyimpan data
    public void save(final FavoritModel favoritModel) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(FavoritModel.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    favoritModel.setId(nextId);
                    FavoritModel model = realm.copyToRealm(favoritModel);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }

    // untuk memanggil semua data
    public List<FavoritModel> getAllFavorit() {
        RealmResults<FavoritModel> results = realm.where(FavoritModel.class).findAll();
        return results;
    }

    // untuk meng-update data
    public void update(final Integer id, final String nama, final String kategori, final String jenis, final String foto) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                FavoritModel model = realm.where(FavoritModel.class)
                        .equalTo("id", id)
                        .findFirst();
                model.setNama(nama);
                model.setKategori(kategori);
                model.setJenis(jenis);
                model.setFoto(foto);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.e("", "onSuccess: Update Successfully");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });
    }


    // untuk menghapus data
    public void delete(Integer id) {
        final RealmResults<FavoritModel> model = realm.where(FavoritModel.class).equalTo("id", id).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                model.deleteFromRealm(0);
            }
        });
    }

    public void save_warung(final Warung warung) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (realm != null) {
                    Log.e("Created", "DB was created");
                    Number currentIdNum = realm.where(Warung.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }
                    warung.setId(nextId);
                    realm.copyToRealm(warung);
                } else {
                    Log.e("", "DB not Found");
                }
            }
        });
    }
}
