package com.example.rafanabila.pr.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafanabila.pr.Items;
import com.example.rafanabila.pr.Model.FavoritModel;
import com.example.rafanabila.pr.R;
import com.example.rafanabila.pr.RealmHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private List<Items.mealList> listitemList;

    private Context context;
    Realm realm;
    RealmHelper realmHelper;

    FavoritModel favoritModel;
    public Adapter(List<Items.mealList> listitemList, Context context) {
        this.listitemList = listitemList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vertical_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("DataData", listitemList.size() + "");
        //Listitem variable
        final Items.mealList Listitem = listitemList.get(position);
        Log.d("DataData", listitemList.toString());
        //judul diganti nma yang di list
        holder.strMeal.setText(Listitem.getStrMeal());
        holder.strCategory.setText(Listitem.getStrCategory());
        holder.strArea.setText(Listitem.getStrArea());
        Picasso.get().load(Listitem.getStrMealThumb()).into(holder.strMealThumb);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new RealmHelper(realm);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritModel = new FavoritModel();
                favoritModel.setNama(Listitem.getStrMeal());
                favoritModel.setKategori(Listitem.getStrCategory());
                favoritModel.setJenis(Listitem.getStrArea());
                favoritModel.setFoto(Listitem.getStrMealThumb());

                realmHelper = new RealmHelper(realm);
                realmHelper.save(favoritModel);
                Toast.makeText(context, "Menjadikan favorit", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listitemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView strMeal, strCategory, strArea;
        public ImageView strMealThumb;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            strMeal = view.findViewById(R.id.judul);
            strCategory = view.findViewById(R.id.genre);
            strArea = view.findViewById(R.id.harga);
            strMealThumb = view.findViewById(R.id.foto);
            cardView = view.findViewById(R.id.card);
        }
    }

}
