package com.example.rafanabila.pr.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafanabila.pr.Items2;
import com.example.rafanabila.pr.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapterdua extends RecyclerView.Adapter<Adapterdua.ViewHolder> {

    public TextView judul, genre, harga;
    public ImageView foto;
    public CardView card;
    private Context context;
    private List<Items2.categoriesList> listitemduaList;

    public Adapterdua(List<Items2.categoriesList> listitemduaList, Context context) {
        this.listitemduaList = listitemduaList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Items2.categoriesList listitem = listitemduaList.get(position);
        holder.strCategory.setText(listitem.getStrCategory());
        Picasso.get().load(listitem.getStrCategoryThumb()).into(holder.strCategoryThumb);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "" + listitem.getStrCategory(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {

        return listitemduaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView strCategory;
        public ImageView strCategoryThumb;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            strCategory = view.findViewById(R.id.judul);
            strCategoryThumb = view.findViewById(R.id.foto);
            cardView = view.findViewById(R.id.card);
        }
    }
}
