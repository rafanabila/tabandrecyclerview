package com.example.rafanabila.pr;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafanabila.pr.Rest.Preference;

public class UserProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);

        final EditText data = findViewById(R.id.isi);
        final EditText email = findViewById(R.id.email);
        final EditText alamat = findViewById(R.id.alamat);
        final Button save_data = findViewById(R.id.btn_save);
        final Button show_data = findViewById(R.id.btn_show);
//        Button delete_data = findViewById(R.id.btn_del);

        Preference pr = new Preference();
        String data2 = Preference.getData(getApplicationContext());
        String email2 = Preference.getEmail(getApplicationContext());
        String alamat2 = Preference.getAlamat(getApplicationContext());
        data.setText("" + data2);
        email.setText("" + email2);
        alamat.setText("" + alamat2);


        save_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Preference pr = new Preference();
                Preference.setData(getApplicationContext(), data.getText().toString());
                Preference.setEmail(getApplicationContext(), email.getText().toString());
                Preference.setAlamat(getApplicationContext(), alamat.getText().toString());
                if (!data.getText().toString().equals("") &&
                        !email.getText().toString().equals("") &&
                        !alamat.getText().toString().equals("")) {
                    Toast.makeText(UserProfile.this, "Save data success!!!!!!!", Toast.LENGTH_SHORT).show();
                    String data2 = Preference.getData(getApplicationContext());
                    String email2 = Preference.getEmail(getApplicationContext());
                    String alamat2 = Preference.getAlamat(getApplicationContext());
                    data.setText("" + data2);
                    email.setText("" + email2);
                    alamat.setText("" + alamat2);

                    data.setEnabled(false);
                    email.setEnabled(false);
                    alamat.setEnabled(false);
                    save_data.setVisibility(View.GONE);
                    show_data.setVisibility(View.VISIBLE);
                }

            }
        });

        show_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.setEnabled(true);
                email.setEnabled(true);
                alamat.setEnabled(true);
                save_data.setVisibility(View.VISIBLE);
                show_data.setVisibility(View.GONE);
            }
        });

//        delete_data.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Preference.deleteData(getApplicationContext());
//                Toast.makeText(MainActivity.this, "data deleted!!!!!!!", Toast.LENGTH_SHORT).show();
//            }
//        });

    }
}