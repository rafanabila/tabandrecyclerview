package com.example.rafanabila.pr;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafanabila.pr.Adapter.Adapter;
import com.example.rafanabila.pr.Adapter.Adapterdua;
import com.example.rafanabila.pr.Rest.ApiClient;
import com.example.rafanabila.pr.Rest.ApiInterface;
import com.example.rafanabila.pr.Rest.ApiInterface2;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_satu extends Fragment {

    List<Items.mealList> listitemList = new ArrayList<>();
    RecyclerView recyclerView2, recyclerView3;
    Adapter a;
    Adapterdua ad;

    public fragment_satu() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View r = inflater.inflate(R.layout.fragment_satu, container, false);
        recyclerView2 = r.findViewById(R.id.recyclerview2);
        recyclerView3 = r.findViewById(R.id.recyclerview3);
        //recyclerView = r.findViewById(R.id.recyclerview);
        //a = new Adapter(listitemList, getContext());
        //recyclerView.setHasFixedSize(true);

        // recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerView.setAdapter(a);
        filldata();
        return r;

    }

    private void filldata() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Items> call = apiService.getData();

        ApiInterface2 apiService2 = ApiClient.getClient().create(ApiInterface2.class);
        Call<Items2> call2 = apiService2.getData();

        call2.enqueue(new Callback<Items2>() {
            @Override
            public void onResponse(Call<Items2> call2, Response<Items2> response) {
                ArrayList<Items2.categoriesList> categoriesLists = response.body().getCategories();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView2.setLayoutManager(layoutManager);
                Adapterdua adapter = new Adapterdua(categoriesLists, getContext());
                recyclerView2.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Items2> call2, Throwable t) {

            }
        });
        call.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                ArrayList<Items.mealList> mealLists = response.body().getMeals();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerView3.setLayoutManager(layoutManager);
                Adapter adapter = new Adapter(mealLists, getContext());
                recyclerView3.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {

            }
        });

    }
}
